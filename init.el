

(deftheme sdb
  "Created 2014-10-31.")


;;;;;;;;;;;;;;;;;;;;;;;; Tema:



(provide-theme 'sdb)
(require 'color-theme)
(color-theme-initialize)
(color-theme-comidia)

(global-linum-mode t)

(add-hook 'lisp-mode-hook '(lambda ()
  (local-set-key (kbd "RET") 'newline-and-indent)))
  
    (cua-mode t)
    (setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
    (transient-mark-mode 1) ;; No region when it is not highlighted
    (setq cua-keep-region-after-copy t) ;; Standard Windows behaviour
(custom-set-variables)
(custom-set-faces)





;;;;;;;;;;;;;;;;;;;;;;;;;;;;Plugin:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;ServerDownload:

;;Melpa
(when
    (load
     (expand-file-name "~/.emacs.d/package.el/package.el")))
(require 'package)
;; Any add to list for package-archives (to add marmalade or melpa) goes here
(add-to-list 'package-archives 
    '("marmalade" .
      "https://marmalade-repo.org/packages/"))
(package-initialize)





;;;;;;;;;;;;;;;;;;;;;;;;;;;Auto-complete

(add-to-list 'load-path "~/.emacs.d/elpa/auto-complete-1.4")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/elpa/auto-complete-1.4/dict")
(ac-config-default)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;Yasnippet
(require 'yasnippet)
(yas-global-mode 1)

;;Conflitto Yasnippet Auto-Complete
(setq ac-source-yasnippet nil)
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
(define-key yas-minor-mode-map (kbd "<backtab>") 'yas-expand)


;;;;;;;;;;;;;;;;;;;;;;;;;;;Iedit

;;(require 'iedit)

